﻿using System;
using System.Threading.Tasks;
using UBS.TeacherDatabase.Abstraction.Models;

namespace UBS.TeacherDatabase
{
    public class TeacherService : ITeacherService
    {
        private readonly IRepository<Teacher> repo = null;

        public TeacherService(IRepository<Teacher> repo)
        {
            this.repo = repo ?? throw new ArgumentNullException(nameof(repo));
        }
        public async Task<Teacher> GetById(string id)
        {
            return await repo.Get(id);
        }

        public async Task<Teacher> Create(Teacher teacher)
        {

            await repo.Store(teacher);
            return teacher;
        }

        public async Task<Teacher> Update(Teacher teacher)
        {
            await repo.Replace(teacher);
            return teacher;

        }

        public async Task<long> Delete(string id)
        {
            return await repo.Delete(id);
        }

    }
}
