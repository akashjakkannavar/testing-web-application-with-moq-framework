using System;
using Xunit;
using UBS.TeacherDatabase.Abstraction.Models;
using UBS.TeacherDatabase;
using UBS.TeacherDatabase.Api;
using UBS.TeacherDatabase.Persistence;
using Moq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TeacherDatabaseTest
{
    public class TeacherTest
    {
        private readonly ITeacherService teacherService;
        private readonly Mock<IRepository<Teacher>> mockedRepository;
        public TeacherTest()
        {
            mockedRepository = new Mock<IRepository<Teacher>>();
            teacherService = new TeacherService(mockedRepository.Object);
        }

        [Fact]
        public async Task GetTest()
        {
            var teacher = new Teacher
            {
                Id = "5d84aa1f07c6423bd1a6a9f2",
                name = "Ajay",
                desig = "Prof",
                Age = 23,
                salary = 40000
            };

            mockedRepository
            .Setup(repo => repo.Get(It.Is<string>(Id => Id == "5d84aa1f07c6423bd1a6a9f2")))
            .Returns(Task.FromResult<Teacher>(teacher));
            //act
            var result = await teacherService.GetById("5d84aa1f07c6423bd1a6a9f2");

            //assert
            Assert.Equal(JsonConvert.SerializeObject(teacher), JsonConvert.SerializeObject(result));
        }
        [Fact]
        public async Task DeleteTest()
        {
            mockedRepository
                .Setup(repo => repo.Delete(It.Is<string>(Id => Id == "5d84b66507c6423bd1a6a9f6")))
                .Returns(Task.FromResult<long>(1));
            var result = await teacherService.Delete("5d84b66507c6423bd1a6a9f6");
            Assert.Equal(1, result);
        }
        [Fact]
        public async Task CreateTest()
        {
            var teacher = new Teacher()
            {
                Id = "5d84aa1f07c6423bd1a6a9f2",
                name = "Ajay",
                desig = "Prof",
                Age = 23,
                salary = 40000
            };
            mockedRepository
                .Setup(repo => repo.Store(It.Is<Teacher>(Id => teacher.Id == "5d84aa1f07c6423bd1a6a9f2")))
                .Returns(Task.FromResult<Teacher>(teacher));
            var result = await teacherService.Create(teacher);
            Assert.Equal(teacher, result);
        }

        [Fact]
        public async Task UpdateTest()
        {
            var teacher = new Teacher()
            {
                Id = "5d84aa1f07c6423bd1a6a9f2",
                name = "Ajay",
                desig = "Prof",
                Age = 23,
                salary = 40000
            };
            mockedRepository
                .Setup(repo => repo.Replace(It.Is<Teacher>(Id => teacher.Id == "5d84aa1f07c6423bd1a6a9f2")))
                .Returns(Task.FromResult<Teacher>(teacher));
            var result = await teacherService.Update(teacher);
            Assert.Equal(teacher, result);
        }
 
    }
}
