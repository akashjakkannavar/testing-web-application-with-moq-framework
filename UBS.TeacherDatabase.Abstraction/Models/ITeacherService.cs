﻿using System.Threading.Tasks;
using System.Collections.Generic;
using System.Text;

namespace UBS.TeacherDatabase.Abstraction.Models
{
    public interface ITeacherService
    {
        Task<Teacher> GetById(string id);
        Task<Teacher> Update(Teacher teacher);
        Task<Teacher> Create(Teacher teacher);
        Task<long> Delete(string id);
    }
}
