﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UBS.TeacherDatabase.Abstraction.Models
{
    public interface IEntity
    {
        string Id { get; set; }
    }
}
