﻿using System;
using System.Collections.Generic;
using System.Text;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;
using MongoDB.Driver;
using System.ComponentModel.DataAnnotations;

namespace UBS.TeacherDatabase.Abstraction.Models
{
    public class Teacher : IEntity
    {
        [BsonId]
        [Required]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [Required(ErrorMessage = "Employee name is must")]
        public string name { get; set; }
        [Required (ErrorMessage ="Desig is must")]
        public string desig { get; set; } 
        public int Age { get; set; }
        public int salary { get; set; }
    }
}
