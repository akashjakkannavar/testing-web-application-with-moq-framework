﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace UBS.TeacherDatabase.Abstraction.Models
{
    public interface IRepository<TEntity> where TEntity:IEntity
    {
        Task<long> Delete(string id);
        Task<TEntity> Get(string id);
        Task<TEntity> Store(TEntity entity);
        Task<TEntity> Replace(TEntity entity);
    }
}
