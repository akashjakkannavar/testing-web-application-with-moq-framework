﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UBS.TeacherDatabase.Abstraction.Models;

namespace UBS.TeacherDatabase.Api.Controllers
{
    [Route("api/teacher")]
    [ApiController]
    public class TeacherController : ControllerBase
    {
        public TeacherController(TeacherService teacherService)
        {
            TeacherService = teacherService ?? throw new ArgumentNullException(nameof(teacherService));
        }
        public TeacherService TeacherService { get; }
        // GET api/teacher/5
        [HttpGet]
        [Route("{id}")]
        public async Task<ActionResult> Get([FromRoute] string Id)
        {
            return Ok(await TeacherService.GetById(Id));
        }

        // POST api/teacher
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] Teacher teacher)
        {
            return Ok(await TeacherService.Create(teacher));
        }

        // PUT api/teacher
        [HttpPut]
        public async Task<ActionResult> Update([FromBody] Teacher teacher)
        {
            return Ok(await TeacherService.Update(teacher));
        }

        // DELETE api/teacher/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(string id)
        {
            return Ok(await TeacherService.Delete(id));
        }
    }
}
