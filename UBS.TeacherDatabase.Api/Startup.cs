﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using UBS.TeacherDatabase.Persistence;
using UBS.TeacherDatabase.Api;
using UBS.TeacherDatabase.Abstraction.Models;

namespace UBS.TeacherDatabase.Api
{
    public class Startup
    {
        public Startup()
        {

        }
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddTransient<IDatabaseSettings>(p =>
            {
                return new DatabaseSettings
                {
                    DatabaseName = Environment.GetEnvironmentVariable("DATABASE_NAME"),
                    ConnectionString = Environment.GetEnvironmentVariable("CONNECTION_STRING")
                };
            });

            services.AddTransient<IRepository<Teacher>, MongoRepository<Teacher>>();
            services.AddTransient<TeacherService>();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
