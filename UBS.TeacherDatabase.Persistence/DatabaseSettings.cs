﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UBS.TeacherDatabase.Persistence
{
    public class DatabaseSettings : IDatabaseSettings
    {
        public string ConnectionString {get; set;}
        public string DatabaseName { get; set; }
    }
    public interface IDatabaseSettings
    {
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
    }
}
